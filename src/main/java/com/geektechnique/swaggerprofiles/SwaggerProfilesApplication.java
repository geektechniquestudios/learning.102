package com.geektechnique.swaggerprofiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaggerProfilesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerProfilesApplication.class, args);
    }

}
